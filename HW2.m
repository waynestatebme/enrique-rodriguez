A=[1:2:7;linspace(14,56,4);7*ones(1,4);15:11:48]; %Creating matrix
A=A' %messing with it

A(2,3) %All #2. It's just calling elements out of the matrix
A(1,4)
A(:,2)
A(3,:)
D=[A(2,:);A(4,:)]
C=[A(:,1),A(:,4)]
B=[A(2,1),A(2,2),A(2,3);A(3,1),A(3,2),A(3,3);A(4,1),A(4,2),A(4,3)]
E=[A(2,2),A(2,3);A(3,2),A(3,3)]
F=[A(1,2),A(1,4);A(4,2),A(4,4)]

G=[linspace(4,14,3);zeros(1,3);10:-3:4]

H=[0:2:14];
I=[1:2:7];
J=[H(7),H(1),H(8);I(4),I(3),H(1);H(6),I(2),H(3)];
K=repmat(J,2,3)

L=[3,10;-12,16];%#4
M=[19,-11;8,7];
L./M
L/M'
M.\L
M\L'

N=[1,2,5,9,14,20,27,35]%#5
O=N.^2.*exp(N)+N.*(N+log10(N)).^(1/2)
P=N.^3+N.*log(N)-N.^(1/4)

hold on %adding to same graph
T=[0:10:50];%initializing variables
V1=[0,15,8,20,29,24];
V2=[0,7,19,31,40,49];
title('Velocity Vs. Time')%Formatting
xlabel('Time_{Seconds}')
ylabel('Velocity_{m/s}')
plot(T,V1,'b',T,V2,'r')%Plotting the lines and colors
legend('Car 1','Car 2')%Making the legend

figure %making a new graph
Q= @(x) cos(x).*sin(x)+exp(x).*log10(x);%#7
fplot(Q,[0,2*pi])
title('Population Vs. Time')
xlabel('Time_{Years since 1940}')
ylabel('Population_{Millions}')

figure %new graph
hold on
Z=[0:pi/8:(3*pi)/8]; %this is the selected x values
R=cos(Z); %all the y values
S=exp(Z);
U=sin(Z);
V=log(Z);
W=tan(Z);
X=log10(Z);
subplot(2,2,1) %the graphs
plot(Z,R,Z,S)
legend('Cos(x)','e^x')
title('cos(x) Vs e^x')
subplot(2,2,2)
plot(Z,U,Z,V)
legend('sin(x)','ln(x)')
title('sin(x) Vs ln(x)')
subplot(2,2,3)
plot(Z,W,Z,X)
title('tan(x) Vs log(x)')
legend('tan(x)','log(x)')
subplot(2,2,4)
plot(Z,U,Z,R)
title('sin(x) Vs cos(x)')
legend('sin(x)','cos(x)')